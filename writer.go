// Package buffered provides buffered I/O using a buffer file to copy from a single source to multiple consumers.
// The buffer file enables decoupling the source and the consumers (sinks) so that slow consumers don't affect reading
// from the source nor writing to faster consumers.
package buffered

import (
	"io"
	"io/ioutil"
	"os"
	"sync"
)

const (
	bufferFileMode        = 0600
	defaultReadBufferSize = 2 << 14 // 32 KiB
)

// Writer can be used to read from a single source and write to multiple consumers. Writer uses a temporary buffer file to decouple the throughput of the source from the throughput of the sink(s).
type Writer struct {

	// The directory to use for the temporary buffer files.
	// If Tmpdir is empty, Writer uses the default directory for temporary files (see os.TempDir)
	Tmpdir string

	// The size of the in memory buffer to use when reading from the source. Default: 32 KiB
	ReadBufferSize int
}

// Copy reads from the given src and writes to all the consumers using a temporary buffer file.
// Copy closes the src after reading from it successfully. Copy also closes all consumers as soon as the content has been successfully written.
// Copy returns a pair of channels. The first channel is the "done" channel and will receive a value after the last consumer finished.
// The second channel receives all errors that occured during the copy operation.
func (w *Writer) Copy(src io.ReadCloser, consumer ...io.WriteCloser) (<-chan struct{}, <-chan error) {

	// bufferToDisk will close the input when done
	filename, progressChan, bufferErrChan := w.bufferToDisk(src)
	channels := w.multiplexChannels(progressChan, len(consumer))

	errors := make(chan error, len(consumer)+10)
	done := make(chan struct{}, 1)

	var wg sync.WaitGroup
	wg.Add(len(channels))
	for idx, ch := range channels {
		go func(bufferChan <-chan int, cons io.WriteCloser) {
			buf := make([]byte, w.readBufferSize())
			f, err := os.OpenFile(filename, os.O_RDONLY, bufferFileMode)
			if err != nil {
				errors <- err
				return
			}
			defer f.Close()

			for written := range bufferChan {
				n, err := f.Read(buf[:written])
				if err == io.EOF {
					break
				}
				if err != nil {
					errors <- err
					break
				}
				cons.Write(buf[:n])
			}
			cons.Close()
			wg.Done()
		}(ch, consumer[idx])
	}

	go func() {
		// Wait for all consumers to finish
		wg.Wait()

		// Remove the temporary buffer file
		err := os.Remove(filename)
		if err != nil {
			errors <- err
		}
		// Receive all errors from the bufferToDisk call
		err = <-bufferErrChan
		if err != nil {
			errors <- err
		}
		// Signal that we are done writing to _all_ consumers
		done <- struct{}{}
	}()

	return done, errors
}

// bufferToDisk will read from input and write to a temporary buffer file on disk (the filepath will be returned as the first value).
// bufferToDisk will close input when it is done.
func (w *Writer) bufferToDisk(input io.ReadCloser) (string, <-chan int, <-chan error) {
	var filename string
	progressChan := make(chan int, w.numChunks())
	errors := make(chan error, 1)

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer func() {
			close(progressChan)
			close(errors)
		}()
		// Generate content

		tmpfile, err := ioutil.TempFile(w.Tmpdir, "multiwrite-buffer")
		if err != nil {
			errors <- err
			return
		}
		filename = tmpfile.Name()
		tmpfile.Close()

		f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE|os.O_TRUNC, bufferFileMode)
		if err != nil {
			errors <- err
			return
		}
		defer func() {
			err := f.Close()
			if err != nil {
				errors <- err
			}
			err = input.Close()
			if err != nil {
				errors <- err
			}
		}()
		wg.Done() // Done creating the file

		buf := make([]byte, w.readBufferSize())
		for {
			nr, er := input.Read(buf)
			if nr > 0 {
				nw, ew := f.Write(buf[0:nr])
				if ew != nil {
					errors <- ew
					return
				}
				if nr != nw {
					errors <- io.ErrShortWrite
					return
				}
				progressChan <- nw
			}
			if er == io.EOF {
				return
			}
			if er != nil {
				errors <- er
				return
			}
		}
	}()

	// Ensure the file actually exists before we return the filepath
	wg.Wait()
	return filename, progressChan, errors
}

// TODO: Can this be []<-chan int
func (w *Writer) multiplexChannels(in <-chan int, num int) []chan int {
	chans := make([]chan int, num)
	for i := 0; i < num; i++ {
		chans[i] = make(chan int, w.numChunks())
	}

	go func() {
		for v := range in {
			for _, ch := range chans {
				ch <- v
			}
		}
		for _, ch := range chans {
			close(ch)
		}
	}()
	return chans
}

// Return the number of chunks to cater for in the progress channel.
// The buffered progress channels should be large enough that the source can be fully consumed without blocking.
func (w *Writer) numChunks() int {
	return 1000
}

func (w *Writer) readBufferSize() int {
	if w.ReadBufferSize > 0 {
		return w.ReadBufferSize
	}
	return defaultReadBufferSize
}
