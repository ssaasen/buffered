package buffered_test

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"bitbucket.org/ssaasen/buffered"
)

type testSource struct {
	*strings.Reader
}

func (s *testSource) Close() error {
	return nil // NO-OP
}

func NewSource(content string) io.ReadCloser {
	return &testSource{strings.NewReader(content)}
}

type testSink struct {
	*bytes.Buffer
}

func (s *testSink) Close() error {
	return nil // NO-OP
}

func NewSink() *testSink {
	return &testSink{&bytes.Buffer{}}
}

type failingSource struct {
	source     io.ReadCloser
	readFails  bool
	closeFails bool
}

func (s *failingSource) Read(p []byte) (n int, err error) {
	if s.readFails {
		return 0, fmt.Errorf("Failed to read")
	}
	return s.source.Read(p)
}

func (s *failingSource) Close() error {
	if s.closeFails {
		return fmt.Errorf("Failed to close the source")
	}
	return nil // NO-OP
}

func TestCopySingle(t *testing.T) {
	content := "This is a test"
	src := NewSource(content)
	dest := NewSink()

	writer := &buffered.Writer{}
	done, errors := writer.Copy(src, dest)

	<-done

	if len(errors) != 0 {
		t.Errorf("There should not be any errors but got: %d", len(errors))
	}

	if dest.String() != content {
		t.Errorf("Copied content differs. Expected %v, got %v", content, dest.String())
	}
}

func TestCopy(t *testing.T) {
	content := "This is a test"
	src := NewSource(content)
	dest1 := NewSink()
	dest2 := NewSink()

	writer := &buffered.Writer{}
	done, errors := writer.Copy(src, dest1, dest2)

	<-done

	if len(errors) != 0 {
		t.Errorf("There should not be any errors but got: %d", len(errors))
	}

	if dest1.String() != content {
		t.Errorf("Copied content differs. Expected %v, got %v", content, dest1.String())
	}

	if dest2.String() != content {
		t.Errorf("Copied content differs. Expected %v, got %v", content, dest2.String())
	}
}

func TestCopySourceReadFails(t *testing.T) {
	content := "This is a test"
	src := &failingSource{source: NewSource(content), readFails: true}
	dest1 := NewSink()
	dest2 := NewSink()

	writer := &buffered.Writer{ReadBufferSize: 2}
	done, errors := writer.Copy(src, dest1, dest2)

	<-done

	if len(errors) != 1 {
		t.Errorf("There should be one error but got: %d", len(errors))
	}

	if dest1.String() != "" {
		t.Errorf("Copied content should be empty but got %v", dest1.String())
	}

	if dest2.String() != "" {
		t.Errorf("Copied content should be empty but got %v", dest2.String())
	}
}

func TestCopySourceCloseFails(t *testing.T) {
	content := "This is a test"
	src := &failingSource{source: NewSource(content), closeFails: true}
	dest1 := NewSink()
	dest2 := NewSink()

	writer := &buffered.Writer{ReadBufferSize: 2}
	done, errors := writer.Copy(src, dest1, dest2)

	<-done

	if len(errors) != 1 {
		t.Errorf("There should be one error but got: %d", len(errors))
	}

	if dest1.String() != content {
		t.Errorf("Copied content differs. Expected %v, got %v", content, dest1.String())
	}

	if dest2.String() != content {
		t.Errorf("Copied content differs. Expected %v, got %v", content, dest2.String())
	}
}

type slowConsumer struct {
	wrapped io.WriteCloser
	delay   time.Duration
}

func (s *slowConsumer) Write(p []byte) (n int, err error) {
	time.Sleep(s.delay)
	return s.wrapped.Write(p)
}

func (s *slowConsumer) Close() error {
	return s.wrapped.Close()
}

func ExampleWrite() {

	// 1 consumer of a stream
	//          |
	//          v
	// Buffer to file assuming the local filesystem can be written to fast (e.g. /tmp)
	//          |
	//          v
	//       1 to n readers

	filename := "/usr/share/dict/words"
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	out1, err := os.Create("out1")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		out1.Close()
		os.Remove(out1.Name())
	}()

	out2, err := os.Create("out2")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		out2.Close()
		os.Remove(out2.Name())
	}()

	writer := &buffered.Writer{}
	done, errors := writer.Copy(file, &slowConsumer{out1, 20 * time.Millisecond}, &slowConsumer{out2, 100 * time.Millisecond})

	<-done
	fmt.Printf("errors: %d\n", len(errors))
	// Output: errors: 0
}
