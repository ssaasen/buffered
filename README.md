# Buffered

Package buffered provides buffered I/O using a buffer file to copy from a single source to multiple consumers/sinks.
The buffer file enables decoupling the source and the sinks so that slow consumers don't affect reading
from the source nor writing to faster consumers.

                                                                             +---------------------------------+
     +---------------------------------+                              +----->|             Sink 1              |
     |             Source              |-----+                        |      +---------------------------------+
     +---------------------------------+     |                        |
                                             |                        |      +-------------+
                                             |                        +----->|   Sink 2    |
                                             |      +-----------+     |      +-------------+
                                             |      |           |     |
                                             +----->|Buffer file|-----+
                                                    |           |
                                                    +-----------+


                                          buffered.Copy(src, sink1, sink2)


